import { Plugins } from '@capacitor/core';
import { Speaker } from '../models/Speaker';
import { Location } from '../models/Location';
import { Seed, Account } from '../models/Schedule';
import dataUrl from '../assets/data/data.json';
import locationsUrl from '../assets/data/locations.json';

const { Storage } = Plugins;
const HAS_LOGGED_IN = 'hasLoggedIn';
const HAS_SEEN_TUTORIAL = 'hasSeenTutorial';
const USERNAME = 'username';

const STORED_SEEDS = 'stored_seeds';
const ACCOUNTS = 'accounts';

export const getConfData = async () => {
  
  const responseData = await dataUrl;
  const storedSeeds = await getStoredSeeds();
  const speakers = <unknown>responseData.speakers as Speaker[];
  const locations = await locationsUrl as Location[];

  const data = {
    storedSeeds,
    locations,
    speakers,
    filteredTracks: []
  }
  return data;
}

export const getUserData = async () => {
  const response = await Promise.all([
    Storage.get({ key: HAS_LOGGED_IN }),
    Storage.get({ key: HAS_SEEN_TUTORIAL }),
    Storage.get({ key: USERNAME })]);
  const isLoggedin = await response[0].value === 'true';
  const hasSeenTutorial = await response[1].value === 'true';
  const username = await response[2].value || undefined;
  const data = {
    isLoggedin,
    hasSeenTutorial,
    username
  }
  return data;
}

export const setIsLoggedInData = async (isLoggedIn: boolean) => {
  await Storage.set({ key: HAS_LOGGED_IN, value: JSON.stringify(isLoggedIn) });
}

export const setHasSeenTutorialData = async (hasSeenTutorial: boolean) => {
  await Storage.set({ key: HAS_SEEN_TUTORIAL, value: JSON.stringify(hasSeenTutorial) });
}

export const setUsernameData = async (username?: string) => {
  if (!username) {
    await Storage.remove({ key: USERNAME });
  } else {
    await Storage.set({ key: USERNAME, value: username });
  }
}

export const getStoredSeeds = async () => {
  const response = await Promise.all([
    Storage.get({ key: STORED_SEEDS })
  ]);

  let responseData: Array<Seed> = [];

  if (response[0].value) {
    responseData = JSON.parse(await response[0].value);
  }

  return responseData;
}

export const addNewSeed = async (storedSeeds: Seed[], newSeed: Seed) => {
  const updatedItems: Array<Seed> = await getStoredSeeds();
  updatedItems.unshift(newSeed);
  storedSeeds.unshift(newSeed);
  await Storage.set({ key: STORED_SEEDS, value: JSON.stringify(updatedItems) });
}

export const removeSeed = async (key: string) => {
  const updatedItems: Array<Seed> = await getStoredSeeds();
  await Storage.set({ key: STORED_SEEDS, value: JSON.stringify(updatedItems.filter((seed, index) => seed.id != key)) });
}

export const getStoredAccounts = async () => {
  const response = await Promise.all([
    Storage.get({ key: ACCOUNTS })
  ]);

  let responseData: Array<Account> = [];

  if (response[0].value) {
    responseData = JSON.parse(await response[0].value);
  }

  return responseData;
}

export const addNewAccount = async (newAccount: Account) => {
  const updatedItems: Array<Account> = await getStoredAccounts();
  updatedItems.unshift(newAccount);
  await Storage.set({ key: ACCOUNTS, value: JSON.stringify(updatedItems) });
}
