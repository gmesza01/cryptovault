import React, { useState } from 'react';
import { IonHeader, IonToolbar, IonContent, IonPage, IonButtons, IonMenuButton, IonDatetime, IonSelectOption, IonList, IonItem, IonLabel, IonSelect } from '@ionic/react';
import './About.scss';

interface AboutProps { }

const About: React.FC<AboutProps> = () => {

  const [location, setLocation] = useState<'madison' | 'austin' | 'chicago' | 'seattle'>('chicago');
  const [conferenceDate, setConferenceDate] = useState('2047-05-17T00:00:00-05:00');

  const selectOptions = {
    header: 'Select a Location'
  };

  return (
    <IonPage id="about-page">
      <IonContent>
        <IonHeader className="ion-no-border">
          <IonToolbar>
            <IonButtons slot="start">
              <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonButtons slot="end">
            </IonButtons>
          </IonToolbar>
        </IonHeader>

        <div className="about-header">
          {/* Instead of loading an image each time the select changes, use opacity to transition them */}
          <div className="about-image madison" style={{ 'opacity': location === 'madison' ? '1' : undefined }}></div>
          <div className="about-image austin" style={{ 'opacity': location === 'austin' ? '1' : undefined }}></div>
          <div className="about-image chicago" style={{ 'opacity': location === 'chicago' ? '1' : undefined }}></div>
          <div className="about-image seattle" style={{ 'opacity': location === 'seattle' ? '1' : undefined }}></div>
        </div>
        <div className="about-info">
          <h3 className="ion-padding-top ion-padding-start">About</h3>

          <p className="ion-padding-start ion-padding-end">
            <strong>CryptoVault</strong> allows you to securely backup your recovery phrase / mnemonic seed offline by using NFC tags and industry standard cryptography and encryption. <br /> The seed is encrypted using a custom passphrase (we never store these values) and the encrypted information can be written to <strong>NFC tags</strong> using AES 256-bit encryption.
          </p>

          <p className="ion-padding-start ion-padding-end">
            <strong>CryptoVault</strong> allows you to derive multiple accounts from the same mnemonic seed using custom derivation paths and start receiving crypto easily.
            <br />You can export each account as JSON and import to *compatible wallets without the need of exposing your main seed phrase.
          </p>

          <h3 className="ion-padding-top ion-padding-start">What are NFC tags?</h3>

          <p className="ion-padding-start ion-padding-end">
            <ul>
              <li>NFC or Near-Field-Communication is a set of communication protocols for communication between two electronic devices over a distance of 4 cm or less.</li>
              <li>NFC tags are passive devices, which means that they operate without a need of power supply of their own and are dependent on an active device to come into range before they are activated. (such as smartphones)</li>
            </ul>
          </p>

          <h3 className="ion-padding-top ion-padding-start">Supported NFC tags</h3>

          <IonList lines="none">
            <IonItem>
              <IonLabel>
                NXP NTAG216
              </IonLabel>
              <IonLabel className="ion-text-end">
                Yes
              </IonLabel>
            </IonItem>
            <IonItem>
              <IonLabel>
                NXP MIFARE Classic® 1k
              </IonLabel>
              <IonLabel className="ion-text-end">
                Yes
              </IonLabel>
            </IonItem>
            <IonItem>
              <IonLabel>
                Fudan 1k F08
              </IonLabel>
              <IonLabel className="ion-text-end">
                Yes
              </IonLabel>
            </IonItem>
          </IonList>

        </div>
      </IonContent>
    </IonPage>
  );
};

export default React.memo(About);