import React, { useState, useRef } from 'react';

import { IonToolbar, IonContent, IonPage, IonButtons, IonTitle, IonMenuButton, IonButton, IonIcon, IonSearchbar, IonRefresher, IonRefresherContent, IonToast, IonHeader, getConfig, IonGrid, IonRow, IonCol } from '@ionic/react';
import { add, trendingDownOutline, search } from 'ionicons/icons';

import SeedList from '../components/SeedList';
import Mnemonic from '../components/Mnemonic';
import Restore from '../components/Restore';

import { addNewSeed, getStoredSeeds } from '../data/dataApi';
import './MySeedsPage.scss'

import ShareSocialFab from '../components/ShareSocialFab';

import * as selectors from '../data/selectors';
import { connect } from '../data/connect';
import { setSearchText } from '../data/sessions/sessions.actions';
import { Seed } from '../models/Schedule';

interface OwnProps { }

interface StateProps {
  storedSeeds: Seed[];
  mode: 'ios' | 'md'
}

interface DispatchProps {
  setSearchText: typeof setSearchText;
}

type MySeedsPageProps = OwnProps & StateProps & DispatchProps;

const MySeedsPage: React.FC<MySeedsPageProps> = ({ storedSeeds, setSearchText, mode }) => {
  const [showSearchbar, setShowSearchbar] = useState<boolean>(false);
  const ionRefresherRef = useRef<HTMLIonRefresherElement>(null);
  const [showCompleteToast, setShowCompleteToast] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showRestoreModal, setShowRestoreModal] = useState(false);
  const [seedList, _setSeedList] = useState<Array<Seed>>([]);
  const [showToast, setShowToast] = useState(false);
  const [initManualSeedHandler, setInitManualSeedHandler] = useState(true);
  const [initNfcHandler, setInitNfcHandler] = useState(true);
  const [initQrHandler, setInitQrHandler] = useState(true);
  const [searchFilter, setSearchFilter] = useState<string>("");

  const pageRef = useRef<HTMLElement>(null);

  const ios = mode === 'ios';

  React.useEffect(() => {
    console.log('componentDidMount');
    const initialSeedFetch = async () => {
      const response = await getStoredSeeds();
      _setSeedList(response);
    }
    initialSeedFetch();
  }, []);

  const doRefresh = () => {
    setTimeout(() => {
      ionRefresherRef.current!.complete();
      setShowCompleteToast(true);
    }, 2500)
  };

  // Allows to change the parent state from child component
  const onModalClose = () => {
    setShowModal(false);
  }

  const onModalRestoreClose = () => {
    setShowRestoreModal(false);
    setInitManualSeedHandler(true);
    setInitNfcHandler(true);
    setInitQrHandler(true);
  }

  const onManualSeedHandler = (status: boolean) => {
    setInitNfcHandler(true);
    setInitQrHandler(true);
    setInitManualSeedHandler(status);
  }

  const onNfcHandler = (status: boolean) => {
    setInitManualSeedHandler(true);
    setInitQrHandler(true);
    setInitNfcHandler(status);
  }

  const onQrHandler = (status: boolean) => {
    setInitManualSeedHandler(true);
    setInitNfcHandler(true);
    setInitQrHandler(status);
  }

  const onModalSave = async (data: Seed) => {
    console.log('Received encrypted info');
    console.log(data);
    if (data.id && data.name) {
      await addNewSeed(storedSeeds, { id: data.id, name: data.name, encryptedSeed: data.encryptedSeed });
      _setSeedList(await getStoredSeeds());
      console.log(storedSeeds);
    }
    else {
      alert("Empty tag detected!");
    }
    onModalClose();
    onModalRestoreClose();
    setShowToast(true);
  }

  // Temp filter of seed list using search
  const onSearch = async (searchQuery: string) => {
    if (searchQuery != "") {
      const updatedSearch: Array<Seed> = await getStoredSeeds();
      _setSeedList(updatedSearch.filter((seed, index) => seed.name.includes(searchQuery)));
    }
    else {
      _setSeedList(await getStoredSeeds());
    }
  }

  return (
    <IonPage ref={pageRef} id="schedule-page">
      <IonHeader translucent={true}>
        <IonToolbar>
          {!showSearchbar &&
            <IonButtons slot="start">
              <IonMenuButton />
            </IonButtons>
          }
          {!ios && !showSearchbar &&
            <IonTitle>Wallet</IonTitle>
          }
          {showSearchbar &&
            <IonSearchbar showCancelButton="always" placeholder="Search" onIonChange={(e: CustomEvent) => onSearch(e.detail.value)} onIonCancel={() => setShowSearchbar(false)}></IonSearchbar>
          }

          <IonButtons slot="end">
            {!ios && !showSearchbar &&
              <IonButton onClick={() => setShowSearchbar(true)}>
                <IonIcon slot="icon-only" icon={search}></IonIcon>
              </IonButton>
            }
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen={true}>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Wallet</IonTitle>
          </IonToolbar>
          <IonToolbar>
            <IonSearchbar placeholder="Search" onIonChange={(e: CustomEvent) => onSearch(e.detail.value)}></IonSearchbar>
          </IonToolbar>
        </IonHeader>

        <IonGrid>
          <IonRow>
            <IonCol>
              <IonButton onClick={() => setShowModal(true)}>
                <IonIcon slot="start" icon={add} />
                Generate seed
              </IonButton>
              <IonButton onClick={() => setShowRestoreModal(true)}>
                <IonIcon slot="start" icon={trendingDownOutline} />
                Restore seed
              </IonButton>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <Mnemonic status={showModal} onClosehandler={onModalClose} onSaveHandler={onModalSave} />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <Restore status={showRestoreModal} onClosehandler={onModalRestoreClose} onSaveHandler={onModalSave} onManualSeedHandler={onManualSeedHandler} showManualSeedHandlerInit={initManualSeedHandler} onQrHandler={onQrHandler} showQrHandlerInit={initQrHandler} onNfcHandler={onNfcHandler} showNfcHandlerInit={initNfcHandler} />
            </IonCol>
          </IonRow>
        </IonGrid>

        <IonRefresher slot="fixed" ref={ionRefresherRef} onIonRefresh={doRefresh}>
          <IonRefresherContent />
        </IonRefresher>

        <IonToast
          isOpen={showCompleteToast}
          message="Refresh complete"
          duration={2000}
          onDidDismiss={() => setShowCompleteToast(false)}
        />

        <IonToast
          isOpen={showToast}
          duration={3000}
          message="Seed encrypted and saved successfully"
          onDidDismiss={() => setShowToast(false)}
        />

        <SeedList seedList={seedList}></SeedList>
      </IonContent>

      <ShareSocialFab />

    </IonPage>
  );
};

export default connect<OwnProps, StateProps, DispatchProps>({
  mapStateToProps: (state) => ({
    storedSeeds: selectors.getAllSeeds(state),
    mode: getConfig()!.get('mode')
  }),
  mapDispatchToProps: {
    setSearchText
  },
  component: React.memo(MySeedsPage)
});