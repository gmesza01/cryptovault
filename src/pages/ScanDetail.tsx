import React, { useContext, useState } from 'react';
import { withRouter, RouteComponentProps } from 'react-router';
import { match } from "react-router-dom";
import { connect } from '../data/connect';
import { IonHeader, IonToolbar, IonContent, IonPage, IonIcon, IonAccordion, IonToast, IonRow, IonCol, IonButton, IonList, IonItem, IonLabel, IonText, IonTextarea, IonButtons, IonMenuButton, IonTitle, IonAccordionGroup, IonItemDivider } from '@ionic/react';
import './ScanDetail.scss';
import ScanLogo from "../assets/img/appicon.svg";
import { closeCircleOutline, checkmarkCircleOutline } from 'ionicons/icons';

interface OwnProps extends RouteComponentProps { };

interface MatchProps {
    tokenAddress: string;
}

interface StateProps {
    scanParams?: match<MatchProps>
};

type ScanDetailProps = OwnProps & StateProps;

const ScanDetail: React.FC<ScanDetailProps> = ({ scanParams }) => {

    return (
        <IonPage id="scan-detail">
            <IonContent>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton></IonMenuButton>
                        </IonButtons>
                        <IonTitle>Token Analysis</IonTitle>
                    </IonToolbar>
                </IonHeader>

                <div className="login-logo">
                    <img src={ScanLogo} alt="FOMOscan logo" />
                </div>

                <IonTitle>Summary</IonTitle>
                <IonAccordionGroup>
                    <IonAccordion value="trustscore">
                        <IonItem slot="header">
                            <strong>78%</strong>
                            &nbsp;
                            <IonLabel>Trust Score</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Red</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Green</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Blue</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                </IonAccordionGroup>

                <IonItemDivider></IonItemDivider>

                <IonTitle>Swap Analysis</IonTitle>
                <IonAccordionGroup>
                    <IonAccordion value="trade">
                        <IonItem slot="header">
                            <IonIcon icon={checkmarkCircleOutline} />
                            &nbsp;
                            <IonLabel>Can Trade?</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Red</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                    <IonAccordion value="mint">
                        <IonItem slot="header">
                            <IonIcon icon={checkmarkCircleOutline} />
                            &nbsp;
                            <IonLabel>Mint Disabled?</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Yes</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Green</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                </IonAccordionGroup>

                <IonItemDivider></IonItemDivider>

                <IonTitle>Contract Analysis</IonTitle>
                <IonAccordionGroup>
                    <IonAccordion value="sourcecode">
                        <IonItem slot="header">
                            <IonIcon icon={checkmarkCircleOutline} />
                            &nbsp;
                            <IonLabel>Source code verified?</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Red</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                    <IonAccordion value="ownership">
                        <IonItem slot="header">
                            <IonIcon icon={closeCircleOutline} />
                            &nbsp;
                            <IonLabel>Ownership Renounced?</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Yes</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Green</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                </IonAccordionGroup>

                <IonItemDivider></IonItemDivider>

                <IonTitle>Liquidity Analysis</IonTitle>
                <IonAccordionGroup>
                    <IonAccordion value="lppair">
                        <IonItem slot="header">
                            <IonIcon icon={checkmarkCircleOutline} />
                            &nbsp;
                            <IonLabel>LP pair found?</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Red</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                    <IonAccordion value="dex">
                        <IonItem slot="header">
                            <IonIcon icon={closeCircleOutline} />
                            &nbsp;
                            <IonLabel>DEX pairs found</IonLabel>
                        </IonItem>

                        <IonList slot="content">
                            <IonItem>
                                <IonLabel>Yes</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Green</IonLabel>
                            </IonItem>
                        </IonList>
                    </IonAccordion>
                </IonAccordionGroup>

                {scanParams}

            </IonContent>

        </IonPage>
    );
};

export default connect<OwnProps, StateProps>({
    component: withRouter(ScanDetail)
})