import React from 'react';
import { IonTabs, IonRouterOutlet, IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react';
import { Route, Redirect } from 'react-router';
import { scanOutline, walletOutline, informationCircle, lockClosedOutline } from 'ionicons/icons';
import MySeedsPage from './MySeedsPage';
import CoinList from './CoinList';
import CoinDetail from './CoinDetail';
import SeedDetail from './SeedDetail';
import About from './About';
import Scan from './Scan';
import ScanDetail from './ScanDetail';

interface MainTabsProps { }

const MainTabs: React.FC<MainTabsProps> = () => {

  return (
    <IonTabs>
      <IonRouterOutlet>
        <Redirect exact path="/tabs" to="/tabs/seeds" />
        {/*
          Using the render method prop cuts down the number of renders your components will have due to route changes.
          Use the component prop when your component depends on the RouterComponentProps passed in automatically.
        */}
        <Route path="/tabs/seeds" render={() => <MySeedsPage />} exact={true} />
        <Route path="/tabs/coins" render={() => <CoinList />} exact={true} />
        <Route path="/tabs/coins/:id" component={CoinDetail} exact={true} />
        <Route path="/tabs/seeds/:id" component={SeedDetail} />
        <Route path="/tabs/coins/seeds/:id" component={SeedDetail} />
        <Route path="/tabs/scan" render={() => <Scan />} exact={true} />
        <Route path="/tabs/scan/:tokenAddress" component={ScanDetail} />
        <Route path="/tabs/about" render={() => <About />} exact={true} />
      </IonRouterOutlet>
      <IonTabBar slot="bottom">
        <IonTabButton tab="seeds" href="/tabs/seeds">
          <IonIcon icon={walletOutline} />
          <IonLabel>Wallet</IonLabel>
        </IonTabButton>
        <IonTabButton tab="coins" href="/tabs/coins">
          <IonIcon icon={lockClosedOutline} />
          <IonLabel>Lock</IonLabel>
        </IonTabButton>
        <IonTabButton tab="scan" href="/tabs/scan">
          <IonIcon icon={scanOutline} />
          <IonLabel>Scan</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  );
};

export default MainTabs;