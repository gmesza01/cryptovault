import React from 'react';
import { IonHeader, IonToolbar, IonTitle, IonContent, IonPage, IonButtons, IonMenuButton, IonGrid, IonRow, IonCol } from '@ionic/react';
import CoinItem from '../components/CoinItem';
import Derive from '../components/Derive';
import ExportAccount from '../components/ExportAccount';
import { Speaker } from '../models/Speaker';
import { Seed, Account } from '../models/Schedule';
import { addNewAccount, getStoredAccounts } from '../data/dataApi';
import { connect } from '../data/connect';
import * as selectors from '../data/selectors';
import './CoinList.scss';

interface OwnProps { };

interface StateProps {
  speakers: Speaker[];
  storedSeeds: Seed[];
};

interface DispatchProps { };

interface CoinListProps extends OwnProps, StateProps, DispatchProps { };

const CoinList: React.FC<CoinListProps> = ({ speakers, storedSeeds }) => {

  const [showDeriveModal, setShowDeriveModal] = React.useState(false);
  const [showExportModal, setShowExportModal] = React.useState(false);
  const [editValues, setEditValues] = React.useState<[Speaker, Seed]>();
  const [accountEditValues, setAccountEditValues] = React.useState<[Speaker, Seed, string]>();
  const [accountList, _setAccountList] = React.useState<Array<Account>>([]);

  React.useEffect(() => {
    console.log('componentDidMount');
    const initialAccountFetch = async () => {
      const response = await getStoredAccounts();
      _setAccountList(response);
    }
    initialAccountFetch();
  }, []);

  const onDeriveNewAccount = (coin: Speaker, seed: Seed) => {
    console.log('Received');
    console.log(coin);
    console.log(seed);
    setEditValues([coin, seed]);
    setShowDeriveModal(true);
  }

  const onExportNewAccount = (coin: Speaker, seed: Seed, derivation_path: string) => {
    console.log('Received export request');
    console.log(coin);
    console.log(seed);
    console.log(derivation_path);
    setAccountEditValues([coin, seed, derivation_path]);
    setShowExportModal(true);
  }

  const onModalDeriveClose = () => {
    setShowDeriveModal(false);
  }

  const onModalExportClose = () => {
    setShowExportModal(false);
  }

  const onModalAddNewAccount = async (account: Account) => {
    console.log(account);
    console.log(storedSeeds);
    console.log('save account object called');
    await addNewAccount(account);
    setShowDeriveModal(false);
    window.location.reload();
  }

  return (
    <IonPage id="speaker-list">
      <IonHeader translucent={true}>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Coins</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent fullscreen={true}>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Coins</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonGrid fixed>
          <IonRow>
            {speakers.map(speaker => (
              <IonCol size="12" size-md="6" key={speaker.id}>
                <CoinItem
                  key={speaker.id}
                  speaker={speaker}
                  storedSeeds={storedSeeds}
                  storedAccounts={accountList}
                  onDeriveNewAccount={onDeriveNewAccount}
                  onExportNewAccount={onExportNewAccount}
                />
              </IonCol>
            ))}
          </IonRow>
          <IonRow>
            <IonCol>
              <Derive status={showDeriveModal} onClosehandler={onModalDeriveClose} onAddNewAccountHandler={onModalAddNewAccount} data={editValues} />
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <ExportAccount status={showExportModal} onClosehandler={onModalExportClose} data={accountEditValues} />
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default connect<OwnProps, StateProps, DispatchProps>({
  mapStateToProps: (state) => ({
    speakers: selectors.getSpeakers(state),
    storedSeeds: selectors.getAllSeeds(state),
  }),
  component: React.memo(CoinList)
});