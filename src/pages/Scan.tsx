import React, { useContext, useState } from 'react';
import { NavContext } from '@ionic/react';
import { IonHeader, IonToolbar, IonContent, IonPage, IonToast, IonRow, IonCol, IonButton, IonList, IonItem, IonLabel, IonText, IonTextarea, IonButtons, IonMenuButton, IonTitle } from '@ionic/react';
import './Scan.scss';
import ScanLogo from "../assets/img/appicon.svg";

interface ScanProps { }

const Scan: React.FC<ScanProps> = () => {

    const { navigate } = useContext(NavContext);
    const [message, setMessage] = useState('');
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [messageError, setMessageError] = useState(false);
    const [showToast, setShowToast] = useState(false);

    const send = (e: React.FormEvent) => {
        e.preventDefault();
        setFormSubmitted(true);
        if (!message) {
            setMessageError(true);
        }
        if (message) {
            setMessage('');
            setShowToast(true);
            setMessageError(false);
            navigate("/tabs/scan/" + message, "none", "pop");
        }
    };

    return (
        <IonPage id="scan-page">
            <IonContent>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton></IonMenuButton>
                        </IonButtons>
                        <IonTitle>Scan</IonTitle>
                    </IonToolbar>
                </IonHeader>

                <div className="login-logo">
                    <img src={ScanLogo} alt="FOMOscan logo" />
                </div>

                <form noValidate onSubmit={send}>
                    <IonList>
                        <IonItem>
                            <IonLabel position="stacked" color="primary">Enter the Token Contract address</IonLabel>
                            <IonTextarea name="message" value={message} spellCheck={false} autocapitalize="off" rows={3} onIonChange={e => setMessage(e.detail.value!)}
                                required>
                            </IonTextarea>
                        </IonItem>

                        {formSubmitted && messageError && <IonText color="danger">
                            <p className="ion-padding-start">
                                Token contract address cannot be empty!
                            </p>
                        </IonText>}
                    </IonList>

                    <IonRow>
                        <IonCol>
                            <IonButton type="submit" expand="block">Scan now</IonButton>
                        </IonCol>
                    </IonRow>
                </form>

            </IonContent>

            <IonToast
                isOpen={showToast}
                duration={3000}
                message="Scanning Token Contract.."
                onDidDismiss={() => setShowToast(false)} />

        </IonPage>
    );
};

export default React.memo(Scan);