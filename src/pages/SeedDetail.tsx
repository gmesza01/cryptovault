import React, { useContext, useCallback } from 'react';
import { NavContext } from '@ionic/react';
import { match } from "react-router-dom";
import { withRouter, RouteComponentProps } from 'react-router';

import AES from 'crypto-js/aes';
import { enc } from 'crypto-js';
import Qrcode from 'qrcode.react';

import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { FileOpener } from '@ionic-native/file-opener';

import { IonAlert, IonHeader, IonToolbar, IonContent, IonPage, IonButtons, IonBackButton, IonButton, IonIcon, IonText, IonList, IonItem, IonLabel } from '@ionic/react';
import { connect } from '../data/connect';
import * as selectors from '../data/selectors';

import { download, trashBin } from 'ionicons/icons';
import './SeedDetail.scss';

import { Seed } from '../models/Schedule';
import { removeSeed } from '../data/dataApi';

import WriteNFC from './../components/WriteNFC';
import EraseNFC from './../components/EraseNFC';

interface OwnProps extends RouteComponentProps { };

interface DecryptedSeedValue {
  value: string;
}

interface DecryptedSeedValues {
  [key: string]: DecryptedSeedValue;
}

interface MatchProps {
  id: string;
}

interface StateProps {
  seed?: Seed;
  match?: match<MatchProps>
};

type SeedDetailProps = OwnProps & StateProps;

const SeedDetail: React.FC<SeedDetailProps> = ({ match, seed }) => {

  const [showAlert, setShowAlert] = React.useState(false);
  const [alertMessage, setAlertMessage] = React.useState("");
  const [showDecryptPrompt, setShowDecryptPrompt] = React.useState(false);
  const [decryptedSeed, setDecryptedSeed] = React.useState<DecryptedSeedValues>({});
  const [isWriteOpen, setIsWriteOpen] = React.useState<boolean>(false);
  const [isEraseOpen, setIsEraseOpen] = React.useState<boolean>(false);

  const { Device, Filesystem } = Plugins;
  const [platform, setPlatform] = React.useState("");
  const { navigate } = useContext(NavContext);

  // Call this function when required to redirect with the back animation
  const redirect = useCallback(
    () => navigate('/tabs/seeds', 'back'),
    [navigate]
  );

  // Initialize device info
  React.useEffect(initializeDeviceInfo, [getDeviceInfo]);

  function initializeDeviceInfo() {
    getDeviceInfo();
  }

  async function getDeviceInfo() {
    const info = await Device.getInfo();
    console.log(info.platform);
    setPlatform(info.platform);
  }

  if (!seed) {
    return <div>Seed details not found!</div>
  }

  async function fileWrite() {
    if (platform == "web") {
      var file = new Blob(["sample text data for web"], { type: "text/plain" });

      let a = document.createElement('a');
      a.href = URL.createObjectURL(file);
      a.download = "seed.txt";
      a.click();
    }
    else {
      try {
        Filesystem.writeFile({
          path: "seed-test.txt",
          data: "sample seed info update IOS ANDROID - new update 12.10",
          directory: FilesystemDirectory.Documents,
          encoding: FilesystemEncoding.UTF8
        }).then((writeFileResult) => {
          const path = writeFileResult.uri;
          FileOpener.open(path, 'text/plain')
            .then(() => console.log('File is opened'))
            .catch(error => console.log('Error openening file', error));
        });
      } catch (error) {
        console.error('Unable to write file', error);
      }
    }
  }

  async function removeSeedItem() {
    removeSeed(match.params.id);
    redirect();
    window.location.reload();
  }

  const sessionClick = (text: string) => {
    console.log(`Clicked ${text}`);
    setAlertMessage(text);
    setShowAlert(true);
  };

  const onModalWriteClose = () => {
    setIsWriteOpen(false);
  }

  const onModalEraseClose = () => {
    setIsEraseOpen(false);
  }

  return (
    <IonPage id="session-detail-page">
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tabs/seeds"></IonBackButton>
          </IonButtons>
          <IonButtons slot="end">
            <IonButton onClick={() => fileWrite()}>
              <IonIcon slot="icon-only" icon={download}></IonIcon>
            </IonButton>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <div className="ion-padding">
          <h1>{platform} - {seed.name}</h1>

          <IonText color="medium">
            <strong>Your Encrypted Seed: </strong><p>{seed.encryptedSeed}</p>
          </IonText>

          <IonText color="dark">
            <p>{
              decryptedSeed[seed.id] ?
                (decryptedSeed[seed.id].value) : ""
            }</p>
          </IonText>

          {
            decryptedSeed[seed.id] && decryptedSeed[seed.id].value != "Failed to decrypt!" ?
              (<IonItem lines='none'>
                <Qrcode
                  className="nfc"
                  value={decryptedSeed[seed.id].value}
                  size={256}
                  bgColor={"#ffffff"}
                  fgColor={"#000000"}
                  level={"L"}
                  includeMargin={false}
                  renderAs={"svg"}
                  imageSettings={{
                    src: "https://www.cryptovault.tech/assets/img/nfc1.png",
                    height: 56,
                    width: 56,
                    excavate: true,
                  }}
                />
              </IonItem>) : ""
          }

        </div>
        <IonList>
          <IonItem onClick={() => setShowDecryptPrompt(true)} button>
            <IonLabel color="primary">Decrypt Seed</IonLabel>
          </IonItem>

          {platform != "web" &&
            <IonItem onClick={() => sessionClick("Making readonly")} button>
              <IonLabel color="primary">Set NFC tag to read only</IonLabel>
            </IonItem> &&
            <IonItem onClick={() => setIsEraseOpen(true)} button>
              <IonLabel color="primary">Erase NFC tag</IonLabel>
            </IonItem>
          }

          {platform != "web" &&
            <IonItem onClick={() => setIsWriteOpen(true)} button>
              <IonLabel color="primary">Backup to NFC tag</IonLabel>
            </IonItem>
          }

          <IonItem onClick={() => removeSeedItem()} button>
            <IonLabel color="primary">Delete Locally</IonLabel>
            <IonIcon slot="end" color="primary" size="small" icon={trashBin}></IonIcon>
          </IonItem>
        </IonList>
      </IonContent>

      <WriteNFC isWriteOpen={isWriteOpen} onClosehandler={onModalWriteClose} seed={seed} version="0.1"></WriteNFC>
      <EraseNFC isEraseOpen={isEraseOpen} onClosehandler={onModalEraseClose} version="0.1"></EraseNFC>

      <IonAlert
        isOpen={showAlert}
        onDidDismiss={() => setShowAlert(false)}
        cssClass='actionAlert'
        header={'Alert'}
        subHeader={'Subtitle'}
        message={alertMessage}
        buttons={['OK']}
      />

      <IonAlert
        isOpen={showDecryptPrompt}
        onDidDismiss={() => setShowDecryptPrompt(false)}
        cssClass='vault-prompt'
        header={'Decrypt seed'}
        inputs={[
          {
            name: 'pass',
            type: 'password',
            placeholder: 'Password to decrypt this seed',
            attributes: {
              inputmode: 'text'
            }
          }
        ]}
        buttons={[
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          },
          {
            text: 'Ok',
            handler: (data) => {
              console.log(data);
              console.log(seed);

              // Decrypt our seed
              let decryptMySeed = AES.decrypt(seed.encryptedSeed, data.pass);

              let decryptMySeedValues: DecryptedSeedValues = {};
              decryptMySeedValues[seed.id] = { value: decryptMySeed.toString(enc.Utf8) || "Failed to decrypt!" };

              // Update state
              setDecryptedSeed(decryptMySeedValues);
              console.log('Confirm Decrypt OK');
            }
          }
        ]}
      />
    </IonPage>
  );
};

export default connect<OwnProps, StateProps>({
  mapStateToProps: (state, OwnProps) => ({
    seed: selectors.getSeedInfo(state, OwnProps)
  }),
  component: withRouter(SeedDetail)
})