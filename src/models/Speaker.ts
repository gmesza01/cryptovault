export interface Speaker {
  id: number;
  name: string;
  machine_name: string;
  profilePic: string;
  twitter: string;
  instagram: string;
  about: string;
  title: string;
  location: string;
  email: string;
  phone: string;
}
