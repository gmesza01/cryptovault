export interface Schedule {
  date: string;
  groups: ScheduleGroup[]
}

export interface Seed {
  id: string,
  name: string,
  encryptedSeed: string
}

export interface Account {
  id: string,
  address: string | undefined,
  derive_path: string,
  account_type: number | undefined,
  seed_id: string | undefined
}

export interface ScheduleGroup {
  time: string;
  sessions: Session[];
}

export interface Session {
  id: number;
  timeStart: string;
  timeEnd: string;
  name: string;
  location: string;
  description: string;
  speakerNames: string[];
  tracks: string[];
}
