import React from 'react';
import { NFC, NdefRecord, NfcTag, NfcUtil, Ndef, NdefEvent } from '@ionic-native/nfc';
import { Plugins } from '@capacitor/core';

import { IonModal, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonContent, IonRow, IonCol } from '@ionic/react';
import { Observable } from '@polkadot/types/types';

import { Seed } from '../models/Schedule';

interface ContainerProps {
    isWriteOpen: boolean;
    onClosehandler: () => void;
    seed?: Seed;
    version: string;
}

const WriteNFC: React.FC<ContainerProps> = ({ isWriteOpen, onClosehandler, seed, version }) => {

    const nfc = NFC;
    const { Device } = Plugins;

    const [platform, setPlatform] = React.useState("");

    // Initialize NFC when the app is started
    React.useEffect(initializeWriteNfc, [getDeviceInfo]);

    function initializeWriteNfc() {
        getDeviceInfo();
    }

    async function getDeviceInfo() {
        const info = await Device.getInfo();
        setPlatform(info.platform);
    }

    async function onWriteSeed() {
        let seedData = [
            Ndef.textRecord(JSON.stringify(seed))
        ];

        // Attempt to write NFC tag
        if (platform == 'ios') {
            try {
                await nfc.write(seedData);
            } catch (error) {
                alert(error);
            }
        }
        else if (platform == 'android') {
            let writeListener = nfc.addNdefListener(() => {
                alert('Ready to scan - hover near writable tag to update.');
            }, (err: any) => {
                alert(err);
            }).subscribe(async (event) => {
                try {
                    await nfc.write(seedData);
                    alert("Wrote seed data to NFC tag");
                    writeListener.unsubscribe();
                } catch (error) {
                    alert(error);
                }
            });
        }
        else {
            alert("No NFC detected");
        }

    }

    return (
        <>
            <IonModal isOpen={isWriteOpen} className='writeNFCModal' onDidDismiss={onClosehandler}>
                <IonHeader translucent={true}>
                    <IonToolbar>
                        <IonTitle>
                            Write NFC tag v{version}
                        </IonTitle>

                        <IonButtons slot="end">
                            <IonButton onClick={onClosehandler} strong>Cancel</IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonRow>
                        <IonCol>
                            <IonButton color='warning' expand='block' onClick={e => onWriteSeed()}>Write now</IonButton>
                        </IonCol>
                    </IonRow>
                </IonContent>
            </IonModal>
        </>
    );

};

export default WriteNFC;