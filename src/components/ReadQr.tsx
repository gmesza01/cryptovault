import React from 'react';

import { IonButton } from '@ionic/react';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

interface ContainerProps {
    name?: string;
}

const ReadQr: React.FC<ContainerProps> = ({ name }) => {

    const [encodedData, setEncodedData] = React.useState("");
    const scanCode = async () => {
        const data = await BarcodeScanner.scan();
        setEncodedData(data.text);
        // @TODO -> encrypt and import?
    };

    return (
        <div className="nfc">
            <img src="assets/img/qr.svg" alt="Scan NFC tag" />
            <div>
                <IonButton onClick={scanCode} color="primary">
                    Scan QR
                </IonButton>

                {
                    encodedData ?
                        (<div>
                            <p>
                                Scanned Code Text: {encodedData} <b></b>
                            </p>
                        </div>) : ''
                }
            </div>
        </div>
    );
};

export default ReadQr;