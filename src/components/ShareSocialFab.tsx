import { IonLoading, IonFab, IonFabButton, IonIcon, IonFabList } from "@ionic/react"
import { chatbox, logoTwitter, logoGitlab, mail } from "ionicons/icons"
import React, { useState } from "react"

const ShareSocialFab: React.FC = () => {
  const [loadingMessage, setLoadingMessage] = useState('')
  const [showLoading, setShowLoading] = useState(false);

  const openSocial = (network: string) => {
    setLoadingMessage(`Redirecting to ${network}`);
    setShowLoading(true);

    let url = "";
    if (network == "Twitter") {
      url = "https://twitter.com";
    }
    else if (network == "GitLab") {
      url = "https://gitlab.com/gmesza01/cryptovault/-/tree/dev";
    }
    else if (network == "E-Mail") {
      url = "mailto:hello@cryptovault.tech";
    }

    let win = window.open(url, '_blank');
    if (win) {
      win.focus();
    }
  };

  return (
    <>
      <IonLoading
        isOpen={showLoading}
        message={loadingMessage}
        duration={2000}
        spinner="crescent"
        onDidDismiss={() => setShowLoading(false)}
      />
      <IonFab slot="fixed" vertical="bottom" horizontal="end">
        <IonFabButton>
          <IonIcon icon={chatbox} />
        </IonFabButton>
        <IonFabList side="top">
          <IonFabButton color="twitter" onClick={() => openSocial('Twitter')}>
            <IonIcon icon={logoTwitter} />
          </IonFabButton>
          <IonFabButton color="vimeo" onClick={() => openSocial('E-Mail')}>
            <IonIcon icon={mail} />
          </IonFabButton>
          <IonFabButton color="instagram" onClick={() => openSocial('GitLab')}>
            <IonIcon icon={logoGitlab} />
          </IonFabButton>
        </IonFabList>
      </IonFab>
    </>
  )
};

export default ShareSocialFab;