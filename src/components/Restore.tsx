import React from 'react';

import { IonModal, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonContent, IonItemDivider, IonIcon, IonItem } from '@ionic/react';
import { qrCodeOutline, scanCircleOutline, readerOutline } from 'ionicons/icons';

import { Seed } from '../models/Schedule';
import ManualSeed from '../components/ManualSeed';
import ReadNFC from '../components/ReadNFC';
import ReadQr from '../components/ReadQr';

import { Plugins } from '@capacitor/core';

interface ContainerProps {
    status: boolean;
    onClosehandler: () => void;
    onSaveHandler: (data: Seed) => void;
    onManualSeedHandler: (status: boolean) => void;
    onQrHandler: (status: boolean) => void;
    onNfcHandler: (status: boolean) => void;
    showManualSeedHandlerInit: boolean,
    showQrHandlerInit: boolean,
    showNfcHandlerInit: boolean
}

const Restore: React.FC<ContainerProps> = ({ status, onClosehandler, onSaveHandler, onManualSeedHandler, onQrHandler, onNfcHandler, showManualSeedHandlerInit, showQrHandlerInit, showNfcHandlerInit }) => {
    const { Device } = Plugins;

    const [platform, setPlatform] = React.useState("");

    // Initialize device info
    React.useEffect(initializeDeviceInfo, [getDeviceInfo]);

    function initializeDeviceInfo() {
        getDeviceInfo();
    }

    async function getDeviceInfo() {
        const info = await Device.getInfo();
        setPlatform(info.platform);
    }

    return (
        <div className="container">
            <IonModal isOpen={status} className='seedModal' onDidDismiss={onClosehandler}>
                <IonHeader translucent={true}>
                    <IonToolbar>
                        <IonTitle>
                            Restore seed
                        </IonTitle>

                        <IonButtons slot="end">
                            <IonButton onClick={onClosehandler} strong>Cancel</IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonItemDivider>Select an option</IonItemDivider>
                    <IonButton onClick={() => onManualSeedHandler(false)}>
                        <IonIcon slot="start" icon={readerOutline} />
                        Enter seed manually
                    </IonButton>

                    {platform != "web" &&
                        <IonButton onClick={() => onNfcHandler(false)}>
                            <IonIcon slot="start" icon={scanCircleOutline} />
                            Read NFC tag
                        </IonButton> &&
                        <IonButton onClick={() => onQrHandler(false)}>
                            <IonIcon slot="start" icon={qrCodeOutline} />
                            Read QR code
                        </IonButton>
                    }

                    <IonContent hidden={showManualSeedHandlerInit}>
                        <ManualSeed onSaveHandler={onSaveHandler} />
                    </IonContent>

                    <IonItem hidden={showNfcHandlerInit} lines="none" className="ion-text-center">
                        <ReadNFC onSaveHandler={onSaveHandler} version="0.12" />
                    </IonItem>

                    <IonItem hidden={showQrHandlerInit} lines="none" className="ion-text-center">
                        <ReadQr />
                    </IonItem>
                </IonContent>
            </IonModal>
        </div>
    );
};

export default Restore;