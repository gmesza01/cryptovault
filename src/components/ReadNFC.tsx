import React from 'react';
import { NFC, NdefRecord, NfcTag, NfcUtil, Ndef, NdefEvent } from '@ionic-native/nfc';
import { Plugins } from '@capacitor/core';
import { IonButton } from '@ionic/react';

import { Seed } from '../models/Schedule';
import './ReadNFC.scss'

interface ContainerProps {
    onSaveHandler: (data: Seed) => void;
    version: string;
}

/** Type for the possible steps of the NFC */
type TStep =
    | "initializing"
    | "nonMobile"
    | "noNfc"
    | "nfcNotEnabled"
    | "waitingForNfcEnabled"
    | "cancelled"
    | "iosRead"
    | "tagRead";

const ReadNFC: React.FC<ContainerProps> = ({ onSaveHandler, version }) => {
    const [step, setStep] = React.useState<TStep>("waitingForNfcEnabled");
    const [tagContent, setTagContent] = React.useState("");
    const nfc = NFC;
    const { Device } = Plugins;

    function initializeNfc() {
        console.log(nfc);
        getDeviceInfo();
    }

    async function getDeviceInfo() {
        const info = await Device.getInfo();
        if (info.platform == 'ios') {
            // Read NFC Tag - iOS
            // On iOS, a NFC reader session takes control from your app while scanning tags then returns a tag
            try {
                let tag = await nfc.scanNdef();
                console.log(JSON.stringify(tag));
                decodeNdefSeed(tag);
            } catch (err) {
                console.log('Error reading tag', err);
                alert(err);
            }
            setStep("iosRead");
        }
        else if (info.platform == 'android') {
            // Read NFC Tag - Android
            let readListener = nfc.addNdefListener(() => {
                alert('Ready to scan - hold near NFC tag to read.');
            }, (err: any) => {
                alert(err);
            }).subscribe(async (event) => {
                decodeNdefSeed(event.tag);
                readListener.unsubscribe();
            });
        }
        else {
            alert("No NFC detected");
        }
    }

    function decodeNdefSeed(tag: NfcTag) {
        if (tag.ndefMessage != undefined) {
            tag.ndefMessage.forEach((record, i) => {
                onSaveHandler(JSON.parse(Ndef.textHelper.decodePayload(record.payload)));
            });
        }
    }

    function onInit(tag: NfcTag) {
        console.log(JSON.stringify(tag));
        decodeNdefSeed(tag);
        setStep("tagRead");
    }

    function onInitError(err: any) {
        console.log('Error reading tag', err);
        //alert(err); @TODO store the error and display
        if (err == "cordova_not_available") {
            setStep("nonMobile");
        }
        else if (err == "NFC_DISABLED") {
            setStep("nfcNotEnabled");
        }
        else if (err == "NO_NFC") {
            setStep("noNfc");
        }
    }

    function onGoToSettingsClick() {
        if (typeof NFC !== "undefined") {
            // Ask the device to open the NFC settings for the user
            NFC.showSettings();
        }
    }

    function onNdefEvent(e: any) {
        // Unregister the event listener
        console.log(e);

        setStep("tagRead");
    }

    function onStopClick() {
        if (typeof NFC !== "undefined") {
            // Unregister the event listener
            //NFCOriginal.removeNdefListener(onNdefEvent);
        }

        setStep("cancelled");
    }

    return (
        <div className="nfc">
            <img src="assets/img/nfc.png" alt="Scan NFC tag" />
            {step === "initializing" ? (
                <div>Scan a NFC Tag to see its content { version}</div>
            ) : step === "nonMobile" ? (
                <div>
                    You need to use our mobile app to be able read NFC tags { version}
                </div>
            ) : step === "noNfc" ? (
                <div>
                    The device you are using doesn't appear to have NFC!
                </div>
            ) : step === "nfcNotEnabled" ? (
                <div>
                    <div>
                        NFC is not enabled on your device. Click the button bellow to open
                        your device's settings, then activate NFC.
					</div>
                    <button onClick={onGoToSettingsClick}>Go to NFC Settings</button>
                </div>
            ) : step === "waitingForNfcEnabled" ? (
                <div>
                    <div>Please click the button below once you have enabled NFC.</div>
                    <IonButton onClick={initializeNfc} color="primary">Read NFC</IonButton>
                </div>
            ) : step === "iosRead" ? (
                <div>
                    <div>IOS read mode.</div>
                </div>
            ) : step === "tagRead" ? (
                <div>
                    <div>Tag scanned! Here it's content:</div>
                    <div>{tagContent}</div>
                    <button onClick={onStopClick}>Stop NFC Reader</button>
                </div>
            ) : (
                                            <div>
                                                <div>Bye!</div>
                                            </div>
                                        )}
        </div>
    );
};

export default ReadNFC;