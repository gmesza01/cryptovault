import React from 'react';

import AES from 'crypto-js/aes';
import { IonItemDivider, IonTextarea, IonItem, IonRow, IonCol, IonButton, IonAlert } from '@ionic/react';

import { Keyring } from '@polkadot/api';
import { mnemonicValidate } from '@polkadot/util-crypto';
import keyring from '@polkadot/ui-keyring';

import { Seed } from '../models/Schedule';


interface ContainerProps {
    name?: string;
    onSaveHandler: (data: Seed) => void;
}

const ManualSeed: React.FC<ContainerProps> = ({ name, onSaveHandler }) => {
    const [publicDotAddress, setPublicDotAddress] = React.useState<string>();
    const [publicKsmAddress, setPublicKsmAddress] = React.useState<string>();
    const [mnemonic, setMnemonic] = React.useState<string>("");
    const [showEncryptPrompt, setShowEncryptPrompt] = React.useState(false);

    async function onRestoreSeed(existingSeed?: string) {

        console.log(existingSeed);

        // Create a keyring instance
        const keyringGen = new Keyring({ type: 'sr25519' });

        // Some mnemonic phrase
        const PHRASE = existingSeed || "";
        setMnemonic(PHRASE);

        // If we have valid phrase
        if (mnemonicValidate(PHRASE)) {
            // Add an account, from mnemonic
            const newPair = keyringGen.addFromUri(`${PHRASE}`);
            console.log(newPair);

            if (newPair !== undefined) {
                // 0 -> Polkadot, 2 -> Kusama
                setPublicDotAddress(keyring.encodeAddress(newPair.address, 0));
                setPublicKsmAddress(keyring.encodeAddress(newPair.address, 2));

                // @TODO -> save account as JSON
                //console.log(keyring.saveAccount(newPair, "test123")); // @TODO check how the accounts are added to json

            }
        }
        else {
            setPublicDotAddress("Non valid seed");
            setPublicKsmAddress("Non valid seed");
        }
    }

    return (
        <>
            <IonItemDivider>Enter your mnemonic seed (your addresses will be derived automatically)</IonItemDivider>
            <IonItem>
                <IonTextarea
                    placeholder="Enter your seed words here"
                    rows={4}
                    value={mnemonic}
                    onIonChange={e => onRestoreSeed(e.detail.value!)}
                >
                </IonTextarea>
            </IonItem>

            <IonItemDivider>Your Polkadot (DOT) address</IonItemDivider>
            <IonItem lines='none'>
                <IonTextarea readonly value={publicDotAddress}></IonTextarea>
            </IonItem>

            <IonItemDivider>Your Kusama (KSM) address</IonItemDivider>
            <IonItem lines='none'>
                <IonTextarea readonly value={publicKsmAddress}></IonTextarea>
            </IonItem>

            <IonRow>
                <IonCol>
                    <IonButton color='success' expand='block' onClick={() => setShowEncryptPrompt(true)}>Import</IonButton>
                </IonCol>
            </IonRow>

            <IonAlert
                isOpen={showEncryptPrompt}
                onDidDismiss={() => setShowEncryptPrompt(false)}
                cssClass='vault-prompt'
                header={'Encrypt seed'}
                inputs={[
                    {
                        name: 'name',
                        type: 'text',
                        placeholder: 'Name this seed'
                    },

                    {
                        name: 'pass',
                        type: 'password',
                        placeholder: 'Password to encrypt this seed',
                        attributes: {
                            inputmode: 'text'
                        }
                    }
                ]}
                buttons={[
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: () => {
                            console.log('Confirm Cancel');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (data) => {
                            console.log(data);
                            console.log(mnemonic);

                            // Encrypt our seed
                            let ciphertext = AES.encrypt(mnemonic, data.pass);
                            onSaveHandler({id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), name: data.name, encryptedSeed: ciphertext.toString() });
                            console.log('Confirm Ok');
                        }
                    }
                ]}
            />
        </>
    );
};

export default ManualSeed;