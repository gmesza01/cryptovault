import React from 'react';
import { Speaker } from '../models/Speaker';
import { Seed, Account } from '../models/Schedule';
import { IonCard, IonCardHeader, IonItem, IonLabel, IonAvatar, IonCardContent, IonList, IonButton, IonRouterLink } from '@ionic/react';


interface CoinItemProps {
  speaker: Speaker;
  storedSeeds: Seed[];
  storedAccounts: Account[];
  onDeriveNewAccount: (coin: Speaker, seed: Seed) => void;
  onExportNewAccount: (coin: Speaker, seed: Seed, derivation_path: string) => void;
}

const CoinItem: React.FC<CoinItemProps> = ({ speaker, storedSeeds, storedAccounts, onDeriveNewAccount, onExportNewAccount }) => {

  function filterAccounts(type: number, seed_id: string) {
    return storedAccounts.filter(account => (account.account_type == type && account.seed_id == seed_id));
  }

  return (
    <>
      <IonCard className="speaker-card">
        <IonCardHeader>
          <IonItem button detail={false} lines="none" className="speaker-item" routerLink={`/tabs/coins/${speaker.id}`}>
            <IonAvatar slot="start">
              <img src={process.env.PUBLIC_URL + speaker.profilePic} alt="Speaker profile pic" />
            </IonAvatar>
            <IonLabel>
              <h2>{speaker.name}</h2>
              <p>{speaker.title}</p>
            </IonLabel>
          </IonItem>
        </IonCardHeader>

        <IonCardContent>
          {storedSeeds.map((seed, index) => (
            <IonList lines="none" key={speaker.id + "__" + seed.name}>
              <IonItem detail={false} routerLink={`/tabs/seeds/${seed.id}`}>
                <IonLabel>
                  <h3><strong>{seed.name}</strong></h3>
                </IonLabel>
              </IonItem>
              {filterAccounts(speaker.id, seed.id).map(account => (
                <IonItem detail={false} key={account.address + "__" + seed.name}>
                  <IonLabel>
                    <h3>{account.address}</h3>
                    <p>derivation path: <strong>{account.derive_path}</strong>
                      <br /> <a href={`https://${speaker.machine_name}.subscan.io/account/${account.address}`} target="_blank">Subscan</a> &nbsp;
                             <IonRouterLink className="underline-custom" onClick={() => onExportNewAccount(speaker, seed, account.derive_path)}>Export to JSON</IonRouterLink>
                    </p>
                  </IonLabel>
                </IonItem>
              ))}
              <IonItem detail={false}>
                <IonLabel>
                  <IonButton onClick={() => onDeriveNewAccount(speaker, seed)}>Derive new account</IonButton>
                </IonLabel>
              </IonItem>
            </IonList>

          ))}


          <IonItem detail={false} routerLink={`/tabs/coins/${speaker.id}`}>
            <IonLabel>
              <h3>More about {speaker.name}</h3>
            </IonLabel>
          </IonItem>

          <IonLabel className="margin-left">
            <ul>
              <li>
                Exported accounts are compatible with the 'Restore JSON' functionality on <a href="https://polkadot.js.org/apps" target="_blank">polkadot.js.org</a>
              </li>
            </ul>
          </IonLabel>


        </IonCardContent>
      </IonCard>
    </>
  );
};

export default CoinItem;