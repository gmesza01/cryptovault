import React from 'react';

import { IonList, IonItemSliding, IonItem, IonLabel } from '@ionic/react';
import { Seed } from '../models/Schedule';

interface ContainerProps {
    seedList: Array<Seed>;
}

const SeedList: React.FC<ContainerProps> = ({ seedList }) => {
    return (
        <div>
            <IonList>
                {seedList.map((seed: Seed, seedIndex: number) => (
                    <IonItemSliding key={seedIndex}>
                        <IonItem routerLink={`/tabs/seeds/${seed.id}`}>
                            <IonLabel>
                                <h3>{seed.name}</h3>
                            </IonLabel>
                        </IonItem>
                    </IonItemSliding>
                ))}
            </IonList>
        </div>
    );
};

export default SeedList;