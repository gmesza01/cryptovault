import React from 'react';

import AES from 'crypto-js/aes';
import Qrcode from 'qrcode.react';
import { IonHeader, IonContent, IonRow, IonCol, IonTextarea, IonItem, IonButton, IonItemDivider, IonModal, IonTitle, IonToolbar, IonButtons, IonAlert } from '@ionic/react';

import { ApiPromise, WsProvider, Keyring } from '@polkadot/api';
import { cryptoWaitReady, mnemonicGenerate, mnemonicValidate } from '@polkadot/util-crypto';
import keyring from '@polkadot/ui-keyring';

import { split, combine } from 'shamirs-secret-sharing-ts'

import { Seed } from '../models/Schedule';

interface ContainerProps {
  status: boolean;
  onClosehandler: () => void;
  onSaveHandler: (data: Seed) => void;
}

const Mnemonic: React.FC<ContainerProps> = ({ status, onClosehandler, onSaveHandler }) => {
  const [publicDotAddress, setPublicDotAddress] = React.useState<string>();
  const [publicKsmAddress, setPublicKsmAddress] = React.useState<string>();
  const [mnemonic, setMnemonic] = React.useState<string>("");
  const [showEncryptPrompt, setShowEncryptPrompt] = React.useState(false);

  React.useEffect(() => {
    console.log('componentDidMount');
    try {
      keyring.loadAll({ ss58Format: 42, type: 'sr25519' });
    } catch (error) {
      console.log(error);
    }
    onCreateSeed();
  }, []);

  async function onStartRPC() {
    const wsProvider = new WsProvider('wss://kusama-rpc.polkadot.io');
    const api = await ApiPromise.create({ provider: wsProvider });

    // Subscribe to the new headers
    await api.rpc.chain.subscribeNewHeads((lastHeader) => {
      console.log(`last block #${lastHeader.number} has hash ${lastHeader.hash}`);
      alert(`last block #${lastHeader.number} has hash ${lastHeader.hash}`);
    });
  }

  async function onCreateSeed(existingSeed?: string) {

    console.log(existingSeed);

    // Create a keyring instance
    const keyringGen = new Keyring({ type: 'sr25519' });

    // Some mnemonic phrase
    const PHRASE = existingSeed || mnemonicGenerate(24);
    setMnemonic(PHRASE);

    // If we have valid phrase
    if (mnemonicValidate(PHRASE)) {

      // first wait until the WASM has been loaded (async init)
      await cryptoWaitReady();

      // Add an account, from mnemonic
      const newPair = keyringGen.addFromUri(`${PHRASE}`);
      console.log(newPair);

      if (newPair !== undefined) {
        // 0 -> Polkadot, 2 -> Kusama
        setPublicDotAddress(keyring.encodeAddress(newPair.address, 0));
        setPublicKsmAddress(keyring.encodeAddress(newPair.address, 2));

        // @TODO -> save account as JSON
        //console.log(keyring.saveAccount(newPair, "test123")); // @TODO check how the accounts are added to json

      }
    }
    else {
      setPublicDotAddress("Non valid seed");
      setPublicKsmAddress("Non valid seed");
    }
  }

  return (
    <div className="container">
      <IonModal isOpen={status} className='seedModal' onDidDismiss={onClosehandler}>
        <IonHeader translucent={true}>
          <IonToolbar>
            <IonTitle>
              New seed
            </IonTitle>

            <IonButtons slot="end">
              <IonButton onClick={onClosehandler} strong>Cancel</IonButton>
            </IonButtons>
          </IonToolbar>
        </IonHeader>

        <IonContent>
          <IonItemDivider>Generated mnemonic phrase</IonItemDivider>
          <IonItem>
            <IonTextarea
              rows={4}
              value={mnemonic}
              readonly
              onIonChange={e => onCreateSeed(e.detail.value!)}>
            </IonTextarea>
          </IonItem>

          <IonItemDivider>Your Polkadot (DOT) address</IonItemDivider>
          <IonItem lines='none'>
            <IonTextarea readonly value={publicDotAddress}></IonTextarea>
          </IonItem>

          <IonItemDivider>Your Kusama (KSM) address</IonItemDivider>
          <IonItem lines='none'>
            <IonTextarea readonly value={publicKsmAddress}></IonTextarea>
          </IonItem>

          <IonItem lines='none'>
            <Qrcode
              className="nfc"
              value={mnemonic}
              size={256}
              bgColor={"#ffffff"}
              fgColor={"#000000"}
              level={"L"}
              includeMargin={false}
              renderAs={"svg"}
              imageSettings={{
                src: "https://www.cryptovault.tech/assets/img/nfc1.png",
                height: 56,
                width: 56,
                excavate: true,
              }}
            />
          </IonItem>

          <IonRow>
            <IonCol>
              <IonButton color='warning' expand='block' onClick={e => onCreateSeed()}>Recreate</IonButton>
            </IonCol>
            <IonCol>
              <IonButton color='success' expand='block' onClick={() => setShowEncryptPrompt(true)}>Save</IonButton>
            </IonCol>
          </IonRow>
        </IonContent>
      </IonModal>

      <IonAlert
        isOpen={showEncryptPrompt}
        onDidDismiss={() => setShowEncryptPrompt(false)}
        cssClass='vault-prompt'
        header={'Encrypt seed'}
        inputs={[
          {
            name: 'name',
            type: 'text',
            placeholder: 'Name this seed'
          },

          {
            name: 'pass',
            type: 'password',
            placeholder: 'Password to encrypt this seed',
            attributes: {
              inputmode: 'text'
            }
          }
        ]}
        buttons={[
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
              console.log('Confirm Cancel');
            }
          },
          {
            text: 'Ok',
            handler: (data) => {
              console.log(data);
              console.log(mnemonic);

              const secret = Buffer.from(mnemonic);
              const shares = split(secret, { shares: 3, threshold: 2 });
              console.log(shares);
              let t0 = shares[0].toString('hex');
              //let t1 = shares[1].toString('hex');
              let t2 = shares[2].toString('hex');
              console.log(t0);
              //console.log(t1);
              console.log(t2);

              const recovered = combine([t0, t2]);
              console.log(recovered.toString()) // 'secret key'

              // Encrypt our seed
              let ciphertext = AES.encrypt(mnemonic, data.pass);
              onSaveHandler({ id: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15), name: data.name, encryptedSeed: ciphertext.toString() });
              console.log('Confirm Ok');
            }
          }
        ]}
      />

      <button hidden={true} onClick={onStartRPC}>Start RPC</button>

    </div >
  );
};

export default Mnemonic;
