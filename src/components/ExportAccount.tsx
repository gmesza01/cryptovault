import React from 'react';

import AES from 'crypto-js/aes';
import { enc } from 'crypto-js';

import { Keyring } from '@polkadot/api';
import { mnemonicValidate } from '@polkadot/util-crypto';
import keyring from '@polkadot/ui-keyring';

import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { FileOpener } from '@ionic-native/file-opener';

import { IonText, IonModal, IonHeader, IonLabel, IonTitle, IonToolbar, IonRow, IonCol, IonButtons, IonButton, IonTextarea, IonContent, IonItem, IonItemDivider, IonInput } from '@ionic/react';

import { Speaker } from '../models/Speaker';
import { Seed, Account } from '../models/Schedule';

const { Device, Filesystem } = Plugins;

interface ContainerProps {
    status: boolean;
    onClosehandler: () => void;
    data?: [Speaker, Seed, string?];
}

const ExportAccount: React.FC<ContainerProps> = ({ status, onClosehandler, data }) => {
    console.log(data);

    const [decryptedSeed, setDecryptedSeed] = React.useState<string>("");
    const [newAddress, setNewAddress] = React.useState<string>();
    const [customDerivationPath, setCustomDerivationPath] = React.useState<string>("");
    const [messageError, setMessageError] = React.useState<string>("");

    React.useEffect(() => {
        console.log('componentDidMount');

        try {
          //  keyring.loadAll({ ss58Format: 42, type: 'sr25519' });
        } catch (error) {
            console.log(error);
        }

    }, []);

    function decryptSeedWithPass(password: string) {
        // Decrypt our seed
        if (data != undefined) {
            let decryptSeedData = AES.decrypt(data[1].encryptedSeed, password);

            // Update state if there is a match
            try {
                setDecryptedSeed(decryptSeedData.toString(enc.Utf8));
                getCustomAccount(decryptSeedData.toString(enc.Utf8), data?.[2]);
            } catch (error) {
                console.log(error);
            }
        }
    }

    async function getCustomAccount(seedData: string, derivationPath?: string) {
        // Create a keyring instance
        const keyringGen = new Keyring({ type: 'sr25519' });
        if (derivationPath) {
            setCustomDerivationPath(derivationPath);
        }

        try {
            const newPair = keyringGen.addFromUri(`${seedData}${derivationPath}`);
            console.log(newPair);
            if (newPair !== undefined) {
                // 0 -> Polkadot, 2 -> Kusama
                setMessageError("");
                setNewAddress(keyring.encodeAddress(newPair.address, data?.[0].id));
                // @TODO -> save account as JSON
                //console.log(keyring.saveAccount(newPair, "test123")); // @TODO check how the accounts are added to json

            }
        } catch (error) {
            console.log(error);
            setMessageError("Non valid derivation path, start with / or // + string");
        }
    }

    async function onExportCustomAccount(derivationPath?: string) {
        // Create a keyring instance
        const keyringGen = new Keyring({ type: 'sr25519' });

        if (derivationPath == "") {
            setMessageError("Please set derivation path");
        }
        else {
            if (derivationPath) {
                setCustomDerivationPath(derivationPath);
            }
            // If we have valid phrase
            if (mnemonicValidate(decryptedSeed)) {
                // Add an account, from mnemonic

                try {
                    const newPair = keyringGen.addFromUri(`${decryptedSeed}${derivationPath}`);
                    console.log(newPair);
                    if (newPair !== undefined) {
                        // 0 -> Polkadot, 2 -> Kusama
                        setMessageError("");
                        setNewAddress(keyring.encodeAddress(newPair.address, data?.[0].id));
                        // @TODO -> save account as JSON
                        let JSONobject = keyring.saveAccount(newPair, "test123"); // @TODO check how the accounts are added to json

                        // Amend the address
                        if (newAddress) {
                            JSONobject.address = newAddress;
                        }

                        const info = await Device.getInfo();
                        if (info.platform == "web") {
                            var file = new Blob([JSON.stringify(JSONobject)], { type: "text/plain" });
                            let a = document.createElement('a');
                            a.href = URL.createObjectURL(file);
                            a.download = newAddress + ".json";
                            a.click();
                        }
                        else {
                            try {
                                Filesystem.writeFile({
                                    path: newAddress + ".json",
                                    data: JSON.stringify(JSONobject),
                                    directory: FilesystemDirectory.Documents,
                                    encoding: FilesystemEncoding.UTF8
                                }).then((writeFileResult) => {
                                    const path = writeFileResult.uri;
                                    FileOpener.open(path, 'text/plain')
                                        .then(() => console.log('File is opened'))
                                        .catch(error => console.log('Error openening file', error));
                                });
                            } catch (error) {
                                console.error('Unable to write file', error);
                            }
                        }
                    }
                } catch (error) {
                    console.log(error);
                    setMessageError("Non valid derivation path, start with / or // + string");
                }
            }
            else {
                setMessageError("Non valid seed");
            }
        }
    }

    function checkSeed(): boolean | undefined {
        if (decryptedSeed.length > 0) {
            return false;
        }
        return true;
    }

    function checkDerivationPath(): boolean | undefined {
        if (messageError.length > 0 || (newAddress == undefined)) {
            return true;
        }
        return false;
    }

    return (
        <div className="container">
            <IonModal isOpen={status} className='seedModal' onDidDismiss={onClosehandler}>
                <IonHeader translucent={true}>
                    <IonToolbar>
                        <IonTitle>
                            Export account {data?.[2]}
                        </IonTitle>

                        <IonButtons slot="end">
                            <IonButton onClick={onClosehandler} strong>Cancel</IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonItemDivider>New {data?.[0].name} account using custom derivation path</IonItemDivider>

                    <IonItem>
                        <IonTextarea
                            placeholder="Decrypt your seed words first"
                            rows={4}
                            value={decryptedSeed}
                            readonly
                        >
                        </IonTextarea>
                    </IonItem>

                    <IonItemDivider>Enter your password to decrypt the seed</IonItemDivider>
                    <IonItem>
                        <IonLabel position="stacked" color="primary">Your Password</IonLabel>
                        <IonInput placeholder="Enter your password" name="password" type="password" onIonChange={e => decryptSeedWithPass(e.detail.value!)}>
                        </IonInput>
                    </IonItem>

                    <IonItem hidden={checkSeed()} lines='none'>
                        <IonLabel position="stacked" color="primary">Custom Derivation Path</IonLabel>
                        <IonInput placeholder="Derivation path" value={data?.[2]} type="text" disabled readonly></IonInput>
                    </IonItem>

                    {messageError && <IonText color="danger">
                        <p className="ion-padding-start">
                            {messageError}
                        </p>
                    </IonText>}

                    <IonItemDivider hidden={checkSeed() || checkDerivationPath()}> Your New {data?.[0].name} address</IonItemDivider>
                    <IonItem lines='none' hidden={checkSeed() || checkDerivationPath()}>
                        <IonTextarea readonly value={newAddress}></IonTextarea>
                    </IonItem>

                    <IonRow hidden={checkSeed() || checkDerivationPath()}>
                        <IonCol>
                            <IonButton color='warning' expand='block' onClick={() => onExportCustomAccount(data?.[2])} strong>Export account</IonButton>
                        </IonCol>
                    </IonRow>

                </IonContent>
            </IonModal>
        </div>
    );
};

export default ExportAccount;