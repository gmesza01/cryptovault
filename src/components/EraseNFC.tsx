import React from 'react';
import { NFC, NdefRecord, NfcTag, NfcUtil, Ndef, NdefEvent } from '@ionic-native/nfc';
import { Plugins } from '@capacitor/core';

import { IonModal, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonContent, IonRow, IonCol } from '@ionic/react';

interface ContainerProps {
    isEraseOpen: boolean;
    onClosehandler: () => void;
    version: string;
}

const EraseNFC: React.FC<ContainerProps> = ({ isEraseOpen, onClosehandler, version }) => {

    const nfc = NFC;
    const { Device } = Plugins;

    const [platform, setPlatform] = React.useState("");

    // Initialize NFC when the app is started
    React.useEffect(initializeEraseNfc, [getDeviceInfo]);

    function initializeEraseNfc() {
        getDeviceInfo();
    }

    async function getDeviceInfo() {
        const info = await Device.getInfo();
        setPlatform(info.platform);
    }

    async function onEraseSeed() {
        let seedData = [
            // Set to empty tag
            Ndef.textRecord(JSON.stringify({}))
        ];

        // Attempt to erase NFC tag
        if (platform == 'ios') {
            try {
                await nfc.write(seedData);
            } catch (error) {
                alert(error);
            }
        }
        else if (platform == 'android') {
            let eraseListener = nfc.addNdefListener(() => {
                alert('Ready to scan - hover near writable tag to erase the seed.');
            }, (err: any) => {
                alert(err);
            }).subscribe(async (event) => {
                try {
                    await nfc.write(seedData);
                    alert("Erased seed data from the NFC tag.");
                    eraseListener.unsubscribe();
                } catch (error) {
                    alert(error);
                }
            });
        }
        else {
            alert("No NFC detected");
        }

    }

    return (
        <>
            <IonModal isOpen={isEraseOpen} className='eraseNFCModal' onDidDismiss={onClosehandler}>
                <IonHeader translucent={true}>
                    <IonToolbar>
                        <IonTitle>
                            Erase NFC tag v{version}
                        </IonTitle>

                        <IonButtons slot="end">
                            <IonButton onClick={onClosehandler} strong>Cancel</IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent>
                    <IonRow>
                        <IonCol>
                            <IonButton color='warning' expand='block' onClick={e => onEraseSeed()}>Erase now</IonButton>
                        </IonCol>
                    </IonRow>
                </IonContent>
            </IonModal>
        </>
    );

};

export default EraseNFC;